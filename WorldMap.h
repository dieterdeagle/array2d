//
// Created by ntr on 13.08.21.
//

#ifndef HELLO_WORLD_WORLDMAP_H
#define HELLO_WORLD_WORLDMAP_H

#include <string>
#include <vector>

template <typename T>
class WorldMap {
public:
    WorldMap(unsigned int width, unsigned int height, T defaultValue);

    unsigned int transformCoordinates(unsigned int x, unsigned int y);
    unsigned int getDataSize() { return data_.size(); }
    T getDataAt(unsigned int x, unsigned int y);
    void setDataAt(unsigned int x, unsigned int y, T value);
private:
    unsigned int width_;
    unsigned int height_;
    std::vector<T> data_;
};

template<typename T>
WorldMap<T>::WorldMap(unsigned int width, unsigned int height, T defaultValue) : width_(width), height_(height) {
    data_.resize(width * height, defaultValue);
}

template<typename T>
unsigned int WorldMap<T>::transformCoordinates(const unsigned int x, const unsigned int y) {
    return (std::min(y, height_ - 1) * width_) + std::min(x, width_ - 1);
}

template <typename T>
T WorldMap<T>::getDataAt(unsigned int x, unsigned int y) {
    return data_[transformCoordinates(x,y)];
}

template <typename T>
void WorldMap<T>::setDataAt(unsigned int x, unsigned int y, T value)
{
    data_[transformCoordinates(x,y)] = value;
}

#endif //HELLO_WORLD_WORLDMAP_H
