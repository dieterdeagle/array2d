#include <iostream>
#include <vector>
#include <string>

int main() {
    const std::vector<std::string> myVector {
        "Blubber",
        "Wubber"
    };
    for (const auto& s : myVector ) {
        std::cout << s << std::endl;
    }
    std::cout << "Hello, World!" << std::endl;
    return 0;
}
