//
// Created by ntr on 13.08.21.
//

#include <gtest/gtest.h>
#include "WorldMap.h"

// Demonstrate some basic assertions.
TEST(HelloTest, BasicAssertions) {
    // Expect two strings not to be equal.
    EXPECT_STRNE("hello", "world");
    // Expect equality.
    EXPECT_EQ(7 * 6, 42);
}

TEST(TransformCoordinates, SimpleTransformShouldBeRight) {
    /*
     *   _0_1_2_
     * 0 |0|1|2|
     *   -------
     * 1 |3|4|5|
     *   -------
     */
    auto map = std::make_shared<WorldMap<std::string>>(3, 2, "hello");

    EXPECT_EQ(map->transformCoordinates(0, 0), 0);
    EXPECT_EQ(map->transformCoordinates(1, 0), 1);
    EXPECT_EQ(map->transformCoordinates(2, 0), 2);

    EXPECT_EQ(map->transformCoordinates(0, 1), 3);
    EXPECT_EQ(map->transformCoordinates(1, 1), 4);
    EXPECT_EQ(map->transformCoordinates(2, 1), 5);
}

TEST(TransformCoordinates, ValuesShouldBeClamped) {
    auto map = std::make_shared<WorldMap<std::string>>(3, 2, "hello");

    EXPECT_EQ(map->transformCoordinates(3, 0), 2);
    EXPECT_EQ(map->transformCoordinates(4, 0), 2);
    EXPECT_EQ(map->transformCoordinates(4, 1), 5);

    EXPECT_EQ(map->transformCoordinates(0, 2), 3);
    EXPECT_EQ(map->transformCoordinates(0, 2), 3);

    EXPECT_EQ(map->transformCoordinates(5, 5), 5);
}

TEST(TestData, CheckVectorSize)
{
    auto map = std::make_shared<WorldMap<std::string>>(3, 2, "hello");
    EXPECT_EQ(map->getDataSize(), 6);
}

TEST(TestData, CheckDefaultValue)
{
    auto map = std::make_shared<WorldMap<std::string>>(3, 2, "hello");
    for (auto i = 0; i < 3; ++i) {
        for (auto j = 0; j < 2; ++j) {
            EXPECT_EQ(map->getDataAt(i, j), "hello");
        }
    }

    EXPECT_EQ(map->getDataAt(5, 7), "hello");
}

TEST(TestData, ShouldSetData) {
    auto map = std::make_shared<WorldMap<std::string>>(3, 2, "hello");
    map->setDataAt(2, 1, "Wurst");
    EXPECT_EQ(map->getDataAt(2,1), "Wurst");

    map->setDataAt(6, 4, "Suppe");
    EXPECT_EQ(map->getDataAt(2,1), "Suppe");
}